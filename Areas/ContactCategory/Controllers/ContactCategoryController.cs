﻿using Demo_Areas.Areas.ContactCategory.Models;
using Microsoft.AspNetCore.Mvc;

namespace Demo_Areas.Areas.ContactCategory.Controllers
{
    [Area("ContactCategory")]
    [Route("ContactCategory/[controller]/[action]")]
    public class ContactCategoryController : Controller
    {
        public IActionResult Index()
        {
            ConatactCategoryModel[] m =new ConatactCategoryModel[5];

            m[0]=new ConatactCategoryModel();
            m[0].Name = "Test1";
            m[0].Description = "Test1";

            m[1] = new ConatactCategoryModel();
            m[1].Description = "Test2";
            m[1].Name = "Test2";

            return View("country_list");
        }
    }
}
